<?xml version="1.0" encoding="utf-8"?>
<!--
Generates notification email to patron when a due date change is made for an item.
For some reason the name of the letter in Alma is called Loan Status Notice
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="mailReason.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>
  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>
      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>
        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->
        <br/>
        <xsl:call-template name="toWhomIsConcerned"/>
        <!-- mailReason.xsl -->
        <div class="messageArea">
          <div class="messageBody">
            <table cellspacing="0" cellpadding="5" border="0">
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <tr>
                <td>
                  <xsl:if test="notification_data/message='RECALL_DUEDATE_CHANGE'">
                    <b>@@recall_and_date_change@@</b>
                    <br/>
                    <b><p>Please put the book in the red slot after you have checked it in.</p></b>
                  </xsl:if>
                  <xsl:if test="notification_data/message='RECALL_ONLY'">
                    <b>@@recall_and_no_date_change@@</b>
                    <br/>
                    <b><p>Please put the book in the red slot after you have checked it in.</p></b>
                  </xsl:if>
                  <xsl:if test="notification_data/message='DUE_DATE_CHANGE_ONLY'">
                    <b>@@message@@</b>
                  </xsl:if>
                  <xsl:if test="notification_data/message='RECALL_CANCEL_RESTORE_ORIGINAL_DUEDATE'">
                    <b>@@cancel_recall_date_change@@</b>
                    <!-- <p>Please check the due date below and renew your book if necessary.</p> -->
                  </xsl:if>
                  <xsl:if test="notification_data/message='RECALL_CANCEL_ITEM_RENEWED'">
                    <b>@@cancel_recall_renew@@</b>
                    <!-- <p>Please check the due date below and renew your book if necessary.</p> -->
                  </xsl:if>
                  <xsl:if test="notification_data/message='RECALL_CANCEL_NO_CHANGE'">
                    <b>@@cancel_recall_no_date_change@@</b>
                    <!-- <p>Please check the due date below and renew your book if necessary.</p> -->
                  </xsl:if>
                  <br/>
                  <br/>
                </td>
              </tr>
              <tr>
                <td>
                  For more information, see
                  <a href="http://www.lancaster.ac.uk/library/using-the-library">Using the Library</a>.
                </td>
                <br/>
                <br/>
              </tr>
              <xsl:if test="notification_data/message='RECALL_DUEDATE_CHANGE'">
                <tr>
                  <td>
                    <b>Recalled Item</b>
                  </td>
                </tr>
              </xsl:if>
              <xsl:if test="notification_data/message='RECALL_ONLY'">
                <tr>
                  <td>
                    <b>Recalled Item</b>
                  </td>
                </tr>
              </xsl:if>
              <tr>
                <td>
                  <table cellpadding="5" class="listing">
                    <xsl:attribute name="style">
                      <xsl:call-template name="mainTableStyleCss"/>
                      <!-- style.xsl -->
                    </xsl:attribute>
                    <tr>
                      <th>Title</th>
                      <th>Author</th>
                      <th>Barcode</th>
                      <th>Original Due Date</th>
                      <th>New Due Date</th>
                    </tr>
                    <xsl:for-each select="notification_data/item_loans/item_loan">
                      <tr>
                        <td>
                          <xsl:value-of select="title"/>
                        </td>
                        <td>
                          <xsl:if test="author!='Default Author'">
                            <xsl:value-of select="author"/>
                          </xsl:if>
                        </td>
                        <td>
                          <xsl:value-of select="barcode"/>
                        </td>
                        <td>
                          <xsl:value-of select="old_due_date"/>
                        </td>
                        <td>
                          <xsl:value-of select="due_date"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
            </table>
            <br/>
            <table>
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <xsl:if test="notification_data/receivers/receiver/user/user_group!='10'">
                <tr>
                  <xsl:choose>
                    <xsl:when test="notification_data/message='DUE_DATE_CHANGE_ONLY'">
                      <td>
                        Please note the new due date.
                      </td>
                    </xsl:when>
                    <!-- <xsl:otherwise>
                      <td>
                        Please note the new due date. You will incur fines
                        at a higher rate if the book is not returned by the new
                        due date.
                      </td>
                    </xsl:otherwise> -->
                  </xsl:choose>
                </tr>
              </xsl:if>
            </table>
            <br/>
            <table>
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <tr>
                <td>Yours sincerely,</td>
              </tr>
              <tr>
                <td>Library User Services</td>
              </tr>
              <tr>
                <td>email: library@lancaster.ac.uk</td>
              </tr>
              <tr>
                <td>tel: (01524)5-92516</td>
              </tr>
            </table>
          </div>
        </div>
        <xsl:call-template name="hr"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
