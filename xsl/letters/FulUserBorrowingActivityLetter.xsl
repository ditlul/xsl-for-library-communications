<?xml version="1.0" encoding="utf-8"?>

<!--
This is used to generate the weekly Patron Circulation Summary,
currently sent on Monday mornings.
-->

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="mailReason.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>
  <xsl:include href="recordTitle.xsl"/>
  
  <xsl:template name="print_pat_exp_text">
    <xsl:param name="pat_ug"/>
    <xsl:param name="pat_time_left"/>
    <xsl:param name="print_pat_expiry_date"/>
    <xsl:param name="pat_exp_month"/>
    <xsl:attribute name="style">
      <xsl:call-template name="bodyStyleCss"/>
      <!-- style.xsl -->
    </xsl:attribute>
    <xsl:choose>
      <xsl:when test="$pat_ug='01'
        or $pat_ug='13'
        or $pat_ug='15'
        or $pat_ug='23'">
        <!-- Undergraduate user groups -->
        <xsl:choose>
          <xsl:when test="$pat_time_left &lt; 30">
            <span style="text-decoration:underline;">
              <p>
                <b>
                  Attention - important information about the end of
                  your course
                </b>
              </p>
            </span>
            <xsl:choose>
              <xsl:when test="$pat_exp_month='06' or $pat_exp_month='07'">
                <p>
                  As your course will shortly be ending, we want to remind
                  you to return your books and pay any outstanding fines.
                  All outstanding debts to the University must be paid in full
                  and all library books returned in order for you to be
                  eligible to attend your degree ceremony. The deadline for
                  inclusion in the graduation ceremony programme is
                  Friday 24 June.
                </p>
                <p>
                  Please check the appropriate section below for the dates
                  by which you need to return your books and pay your fines:
                </p>
                <p><b>If you are ...</b><br/></p>
                <p>
                  <b>a final year student</b> (except BA Fine Arts) - the
                  day after your final exam and by Monday 13 June at the latest.
                </p>
                <p>
                  <b>a final year student BA Fine Arts</b> - the
                  day after your final submission date.
                </p>
                <p>
                  <b>a Study Abroad or Erasmus student</b> - by the end of term.
                </p>
                <p>
                  <b>a Certificate of Higher Education Pre-Medical Studies
                    student</b> - the day after your final exam.
                </p>
                <br/>
                <br/>
              </xsl:when>
              <xsl:otherwise>
                <p>
                  As your undergraduate registration will be ending soon, we
                  want to give you plenty of notice to clear your library
                  account before you leave. You will need to return all your
                  books and pay any outstanding fines before the expiry
                  date of your registration (<xsl:value-of select="$print_pat_expiry_date"/>).
                </p>
                <p>
                  If you have applied for an extension you will need to
                  confirm with the Undergraduate Studies Office that this has
                  been processed.
                </p>
                <br/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <!-- Do nothing -->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$pat_ug='12' or $pat_ug='14' or $pat_ug='24'
        or $pat_ug='29' or $pat_ug='30' or $pat_ug='31' or $pat_ug='32'
        or $pat_ug='02'">
        <!-- postgraduate user groups -->
        <xsl:choose>
          <xsl:when test="$pat_time_left &lt; 30">
            <span style="text-decoration:underline;">
              <p>
                <b>Attention - important information about the end of your
                  registration</b>
              </p>
            </span>
            <p>
              As your postgraduate registration will be ending soon, we
              want to give you plenty of notice to clear your library
              account before you leave. You will need to return all your
              books and pay any outstanding fines before the expiry
              date of your registration (<xsl:value-of select="$print_pat_expiry_date"/>).
            </p>
            <p>
              If you have applied for an extension you will need to
              confirm with the Postgraduate Studies Office that this has
              been processed.
            </p>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <!-- Do nothing -->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$pat_ug='39'">
        <!-- Do nothing -->
        <!-- <p>Affiliate message</p> -->
      </xsl:when>
      <xsl:when test="$pat_ug='03' or $pat_ug='21' or $pat_ug='26' or $pat_ug='33' or $pat_ug='distance_staff'">
        <!-- <p>Staff message, Associates</p> -->
        <xsl:choose>
          <xsl:when test="$pat_time_left &lt; 30">
            <span style="text-decoration:underline;">
              <p>
                <b>Attention - important information about the end of your
                  library registration</b>
              </p>
            </span>
            <p>
              Your library account will expire on
              <xsl:value-of select="$print_pat_expiry_date"/> as this is when
              we have been informed you will be leaving the university. Please
              return your book(s) and pay any outstanding fines before you
              leave. If this information is not correct, please reply to this
              email.
            </p>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <!-- Do nothing -->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$pat_ug='09'">
        <!-- Do nothing -->
        <!-- <p>Reader message</p> -->
      </xsl:when>
      <xsl:when test="$pat_ug='10'">
        <!-- Do nothing -->
        <!-- <p>IDS message</p> -->
      </xsl:when>
      <xsl:when test="$pat_ug='34'">
        <!-- Do nothing -->
        <!-- <p>Emeritus staff message</p> -->
      </xsl:when>
      <xsl:when test="$pat_ug='35'">
        <!-- Do nothing -->
        <!-- <p>Retired academic staff message</p> -->
      </xsl:when>
      <xsl:when test="$pat_ug='36'">
        <!-- Do nothing -->
        <!-- <p>Visitor message</p> -->
      </xsl:when>
      <xsl:when test="$pat_ug='37' or $pat_ug='38'">
        <!-- Do nothing -->
        <!-- <p>Library external borrower & Alumni message</p> -->
        <xsl:choose>
          <xsl:when test="$pat_time_left &lt; 30">
            <span style="text-decoration:underline;">
              <p>
                <b>Attention - important information about the end of your
                  library registration</b>
              </p>
            </span>
            <p>
              Your Lancaster University library account will expire on
              <xsl:value-of select="$print_pat_expiry_date"/>. If you wish to
              renew it, please reply to this email. If not, please return
              your book(s) by the due date and pay any outstanding fines.
            </p>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <!-- Do nothing -->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$pat_ug='40'">
        <!-- Do nothing -->
        <!-- <p>System test message</p> -->
      </xsl:when>
      <xsl:otherwise>
        <!-- Do nothing -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>
      <body>
        <xsl:variable name="notice_cur_date" select="
          concat(
            substring(/notification_data/general_data/current_date,7,4),
            substring(/notification_data/general_data/current_date,4,2),
            substring(/notification_data/general_data/current_date,1,2)
          )"/>
        <xsl:variable name="pat_exp_month" select="
          substring(/notification_data/receivers/receiver/user/expiry_date,4,2)
        "/>
        <xsl:variable name="pat_exp_date" select="
          concat(
            substring(/notification_data/receivers/receiver/user/expiry_date,7,4),
            substring(/notification_data/receivers/receiver/user/expiry_date,4,2),
            substring(/notification_data/receivers/receiver/user/expiry_date,1,2)
          )"/>
        <xsl:variable name="print_pat_exp_date" 
          select="/notification_data/receivers/receiver/user/expiry_date"/>
        <xsl:variable name="pat_user_group" 
          select="/notification_data/user_for_printing/user_group"/>
        <xsl:variable name="pat_exp_ddiff">
          <xsl:call-template name="date-diff">
            <xsl:with-param name="date1" select="$pat_exp_date"/>
            <xsl:with-param name="date2" select="$notice_cur_date"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>
        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->
        <br/>
        <xsl:call-template name="toWhomIsConcerned"/>
        <!-- mailReason.xsl -->
        <div class="messageArea">
          <div class="messageBody">
            <!-- for testing <p>notice_cur_date = <xsl:value-of select="$notice_cur_date"/></p> -->
            <!-- for testing <p>pat_exp_date = <xsl:value-of select="$pat_exp_date"/></p> -->
            <!-- for testing <p>pat_exp_ddiff = <xsl:value-of select="$pat_exp_ddiff"/></p> -->
            <!-- for testing <p>pat_user_group = <xsl:value-of select="$pat_user_group"/></p> -->
            <xsl:if test="$pat_exp_ddiff &lt;= 60">
              <table cellspacing="0" cellpadding="5" border="0">
                <tr>
                  <td>
                    <xsl:call-template name="print_pat_exp_text">
                      <xsl:with-param name="pat_ug" select="$pat_user_group"/>
                      <xsl:with-param name="pat_time_left" select="$pat_exp_ddiff"/>
                      <xsl:with-param name="print_pat_expiry_date" select="$print_pat_exp_date"/>
                      <xsl:with-param name="pat_exp_month" select="$pat_exp_month"/>
                    </xsl:call-template>
                  </td>
                </tr>
              </table>
            </xsl:if>
            <table cellspacing="0" cellpadding="5" border="0">
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <xsl:if test="notification_data/item_loans/item_loan or notification_data/overdue_item_loans/item_loan">
                <tr>
                  <td>
                    <b>
                      You have these books on loan. 
                      If any have been recalled, please return 
                      them promptly by the due date as someone 
                      else has requested them. Fines are charged at £2 per day
                      on overdue recalled books.
                      <br/><br/>
                      <xsl:choose>
                        <xsl:when test="$pat_exp_ddiff &gt; 29">
                          Please renew any non-recalled books which are due back.
                          If your book has been on loan for a year, you will need to
                          return and reissue it.
                        </xsl:when>
                      </xsl:choose>
                      If a book is more than 6
                      weeks overdue, we will invoice you for a replacement.
                    </b>
                    <br/>
                    <br/>
                  </td>
                </tr>
                <xsl:if test="notification_data/overdue_item_loans/item_loan or notification_data/item_loans/item_loan">
                  <tr>
                    <td>
                      <b>Loans</b>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table cellpadding="5" class="listing">
                        <xsl:attribute name="style">
                          <xsl:call-template name="mainTableStyleCss"/>
                          <!-- style.xsl -->
                        </xsl:attribute>
                        <tr>
                          <th width="46%">Title</th>
                          <th width="20%">Author</th>
                          <th width="20%">Barcode</th>
                          <th width="7%">Due Date</th>
                          <th width="7%">Recalled?</th>
                          <th width="7%">Overdue?</th>
                        </tr>
                        <xsl:for-each select="notification_data/overdue_item_loans/item_loan|notification_data/item_loans/item_loan">
                          <xsl:sort select="
                            concat(
                              substring(due_date,7,4), 
                              substring(due_date,4,2),
                              substring(due_date,1,2)
                            )" 
                            order="ascending" 
                            data-type="number"/>
                          <xsl:variable name="due_date_num"
                            select="concat(substring(due_date,7,4),substring(due_date,4,2),substring(due_date,1,2))"/>
                          <tr>
                            <td>
                              <xsl:value-of select="title"/>
                            </td>
                            <td>
                              <xsl:if test="author!='Default Author'">
                                <xsl:value-of select="author"/>
                              </xsl:if>
                            </td>
                            <td>
                              <xsl:value-of select="barcode"/>
                              <xsl:if test="location_code='OUT_RS_REQ'">
                                <br/>(Interlending book)
                              </xsl:if>
                            </td>
                            <xsl:variable name="ddiff">
                              <xsl:call-template name="date-diff">
                                <xsl:with-param name="date1" select="$notice_cur_date"/>
                                <xsl:with-param name="date2" select="$due_date_num"/>
                              </xsl:call-template>
                            </xsl:variable>
                            <td>
                              <xsl:if test="$ddiff &gt;= -7">
                                <b>
                                  <xsl:value-of select="due_date"/>
                                </b>
                              </xsl:if>
                              <xsl:if test="$ddiff &lt; -7">
                                <xsl:value-of select="due_date"/>
                              </xsl:if>
                            </td>
                            <td>
                              <xsl:if test="process_status='RECALL'">
                                <b>Yes</b>
                              </xsl:if>
                            </td>
                            <td>
                              <xsl:if test="$due_date_num &lt; $notice_cur_date">
                                <b>Yes</b>
                              </xsl:if>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </table>
                      <br/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:if>
              <xsl:if test="notification_data/organization_fee_list/string">
                <xsl:if test="substring-before(notification_data/total_fee, ' ') &gt; 0.0">
                  <tr>
                    <br/>
                    <b>
                      We would like to remind you that you have outstanding fines or other charges of
                      &#163;<xsl:value-of select="substring-before(notification_data/total_fee, ' ')"/>. 
                      Please pay these as soon as possible.
                    </b>
                  </tr>
                </xsl:if>
              </xsl:if>
              <xsl:if test="notification_data/receivers/receiver/user/user_group!='10'">
                <tr>
                  <td><br/>
                    Renew your books and check and pay any fines online by signing
                    into My Account on <a href="http://onesearch.lancs.ac.uk">Onesearch</a>.
                  </td>
                </tr>
                <tr>
                  <td>
                    For more information, see our
                    <a href="http://www.lancaster.ac.uk/library/using-the-library">guide to using the Library</a>.
                  </td>
                </tr>
              </xsl:if>
            </table>
            <br/>
            <table>
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <tr>
                <td>Yours sincerely,</td>
              </tr>
              <tr>
                <td>Library User Services</td>
              </tr>
              <tr>
                <td>email: library@lancaster.ac.uk</td>
              </tr>
              <tr>
                <td>tel: (01524)5-92516</td>
              </tr>
            </table>
          </div>
        </div>
        <xsl:call-template name="hr"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
