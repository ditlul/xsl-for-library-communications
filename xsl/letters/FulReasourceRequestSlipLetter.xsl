<?xml version="1.0" encoding="utf-8"?>
<!--
Generates the hold shelf slip for printing. Ex Libris's intentional mispelling of 'Reasource'
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="mailReason.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>
  <xsl:include href="recordTitle.xsl"/>
  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>
      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>
        <xsl:if test="notification_data/request/work_flow_entity/step_type='ON_HOLD_SHELF'">
          <xsl:if test="notification_data/user_for_printing/user_group='12'
            or notification_data/user_for_printing/user_group='30'
            or notification_data/user_for_printing/user_group='distance_staff' ">
            <p>
              <h2>
                <b>Distance Learner - Mailing Required</b>
              </h2>
            </p>
          </xsl:if>
          <xsl:choose>
            <xsl:when test="notification_data/organization_unit/code='Ghana'">
              <p>
                <b>Hold Shelf Slip - Ghana Campus</b>
              </p>
            </xsl:when>
            <xsl:otherwise>
              <p>
                <b>Hold Shelf Slip</b>
              </p>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:if test="notification_data/request/work_flow_entity/step_type!='ON_HOLD_SHELF'">
          <xsl:if test="notification_data/user_for_printing/user_group='12'
            or notification_data/user_for_printing/user_group='30' ">
            <p>
              <h2>
                <b>Distance Learner</b>
              </h2>
            </p>
          </xsl:if>
          <p>
            <b>Resource Request Slip</b>
          </p>
        </xsl:if>

        <h1>
          <xsl:variable name="original_print_name">
            <xsl:value-of select="notification_data/user_for_printing/name"/>
          </xsl:variable>
          <xsl:variable name="print_name">
            <xsl:value-of select="substring-before(notification_data/user_for_printing/name, ',')"/>
          </xsl:variable>
          <xsl:if test="string-length($print_name) = 0">
            <b>Requested By: <xsl:value-of select="$original_print_name"/></b><br/>
          </xsl:if>
          <xsl:if test="string-length($print_name) != 0">
            <b>Requested By: <xsl:value-of select="$print_name"/></b><br/>
          </xsl:if>
          <b>Card No(s):
            <xsl:for-each select="notification_data/user_for_printing/identifiers/code_value">
              <xsl:if test="code='01'">
                <xsl:value-of select="value"/>&#160;
              </xsl:if>
            </xsl:for-each>
          </b>

          <!--
          <xsl:variable name="masked_name">
            <xsl:call-template name="lu-mask-string">
              <xsl:with-param name="string" select="notification_data/user_for_printing/name"/>
              <xsl:with-param name="from" select="2"/>
            </xsl:call-template>
          </xsl:variable>
          <b>Requested By: <xsl:value-of select="$masked_name"/></b><br/>
          -->
          <!-- Barcodes are identifiers of type 01, we want to enumerate them all,
          we have no way of knowing which is the current one -->
          <!--
          <b>Card No(s):
            <xsl:for-each select="notification_data/user_for_printing/identifiers/code_value">
              <xsl:if test="code='01'">
                <xsl:variable name="masked_id">
                  <xsl:call-template name="lu-mask-string">
                    <xsl:with-param name="string" select="value"/>
                    <xsl:with-param name="to" select="-3"/>
                  </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="$masked_id"/>&#160;
              </xsl:if>
            </xsl:for-each>
          </b>
          -->
        </h1>
        <!-- <xsl:call-template name="head"/> -->
        <!-- header.xsl -->
        <div class="messageArea">
          <div class="messageBody">
            <table cellspacing="0" cellpadding="5" border="0">
              <!--
              <tr>
                <td>
                  <b>Request ID: </b>
                  <xsl:value-of select="notification_data/request/selected_inventory_id"/>
                </td>
              </tr>
              <xsl:if test="notification_data/request/selected_inventory_type='ITEM'">
                <tr>
                  <td>
                    <h3>
                      <b>Barcode: </b>
                      <xsl:value-of select="notification_data/phys_item_display/barcode"/>
                    </h3>
                  </td>
                </tr>
              </xsl:if>
              -->
              <tr>
                <td>
                  <br/>
                  <h3>Please check out the book at a self-service point,
                    even if you are only using it in the library.</h3>
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:call-template name="recordTitle"/>
                </td>
              </tr>
              <!--
              <b/>
              <tr>
                <xsl:if test="notification_data/phys_item_display/call_number != ''">
                  <td>
                    <h3>
                      <b>Classmark: </b>
                      <xsl:value-of select="notification_data/phys_item_display/call_number"/>
                    </h3>
                  </td>
                </xsl:if>
              </tr>
              <b/>
              <tr>
                <td>
                  <b>Request type: </b>
                  <xsl:value-of select="notification_data/request_type"/>
                </td>
              </tr>
              -->
            </table>
          </div>
        </div>
        <!-- <xsl:call-template name="lastFooter"/> -->
        <!-- footer.xsl -->
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
