<?xml version="1.0" encoding="utf-8"?>

<!--

Lost Loan letter indicating to patron they are being invoiced.
Issue AA-7

-->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>

  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>

      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>

        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->


        <table cellspacing="0" cellpadding="5" border="0">
          <xsl:attribute name="style">
            <xsl:call-template name="mainTableStyleCss"/>
            <!-- style.xsl -->
          </xsl:attribute>
          <tr>
            <td>
              <p>You will shortly be invoiced for
              the following item(s).</p>
              <p>This will be because
              the book is 6 weeks overdue (1 week overdue, if recalled),</p>
              <p>or you have told us the book is lost</p>
              <p><b>OR</b></p>
              <p>we have been informed that you have left the university.</p>
              <p>Please return the item(s) within 7 days to avoid being invoiced.</p>
              <p>There are full details of the charges on the
              <a href="http://www.lancaster.ac.uk/library/using-the-library/fines-and-charges/lost-books-and-replacement-charges">
                library website</a>.</p>
              <p>Remember to check Your Library Account Weekly Statement for full
                details of all your loans. Please renew any non-recalled books
                which are due back. If your book has been on loan for a year,
                you will need to return and reissue it.</p>
            </td>
          </tr>
        </table>

        <table cellpadding="5" class="listing" border="0">
          <xsl:attribute name="style">
            <xsl:call-template name="mainTableStyleCss"/>
            <!-- style.xsl -->
          </xsl:attribute>

          <xsl:for-each select="notification_data">
            <tr>
              <td>
                <b>Lost Item:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/title"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Author:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/author"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Loan date:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/loan_date"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Due date:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/due_date"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Barcode:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/barcode"/>
              </td>
            </tr>
            <!-- <tr>
              <td>
                <b>Classmark:</b>
              </td>
              <td>
                <xsl:value-of select="phys_item_display/call_number"/>
              </td>
            </tr> -->
          </xsl:for-each>
        </table>
        <br/>

        <xsl:call-template name="lastFooter"/>
        <!-- footer.xsl -->
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>