<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template name="toWhomIsConcerned">
    <p>
      <xsl:attribute name="style">
        <xsl:call-template name="blockStyle"/>
      </xsl:attribute>
      <xsl:for-each select="notification_data">
        @@dear@@ 
        <xsl:value-of select="
          normalize-space(
            concat(
              receivers/receiver/user/first_name,
              ' ',
              receivers/receiver/user/middle_name,
              ' ',
              receivers/receiver/user/last_name,','
            )
          )"
        />
      </xsl:for-each>
    </p>
  </xsl:template>
</xsl:stylesheet>
